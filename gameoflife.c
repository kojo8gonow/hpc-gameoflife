#include <endian.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <sys/time.h>

#define calcIndex(width, x,y)  ((y)*(width) + (x))

long TimeSteps = 100;

void writeVTK2(long timestep, double *data, char prefix[1024], int w, int h) {
  char filename[2048];  
  int x,y; 
  
  int offsetX=0;
  int offsetY=0;
  float deltax=1.0;
  long  nxy = w * h * sizeof(float);  

  snprintf(filename, sizeof(filename), "%s-%05ld%s", prefix, timestep, ".vti");
  FILE* fp = fopen(filename, "w");

  fprintf(fp, "<?xml version=\"1.0\"?>\n");
  fprintf(fp, "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n");
  fprintf(fp, "<ImageData WholeExtent=\"%d %d %d %d %d %d\" Origin=\"0 0 0\" Spacing=\"%le %le %le\">\n", offsetX, offsetX + w, offsetY, offsetY + h, 0, 0, deltax, deltax, 0.0);
  fprintf(fp, "<CellData Scalars=\"%s\">\n", prefix);
  fprintf(fp, "<DataArray type=\"Float32\" Name=\"%s\" format=\"appended\" offset=\"0\"/>\n", prefix);
  fprintf(fp, "</CellData>\n");
  fprintf(fp, "</ImageData>\n");
  fprintf(fp, "<AppendedData encoding=\"raw\">\n");
  fprintf(fp, "_");
  fwrite((unsigned char*)&nxy, sizeof(long), 1, fp);

  for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) {
      float value = data[calcIndex(h, x,y)];
      fwrite((unsigned char*)&value, sizeof(float), 1, fp);
    }
  }
  
  fprintf(fp, "\n</AppendedData>\n");
  fprintf(fp, "</VTKFile>\n");
  fclose(fp);
}


void show(double* currentfield, int w, int h) {
  printf("\033[H");
  int x,y;
  for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) printf(currentfield[calcIndex(w, x,y)] ? "\033[07m  \033[m" : "  ");
    //printf("\033[E");
    printf("\n");
  }
  fflush(stdout);
}

int countNeighbours(double* currentfield, int w, int h, int x, int y){
  int alive = 0;
      for(int i = -1; i<=1; i++){
        for(int j = -1; j<=1; j++){
          int xNeighbour = x + i;
          int yNeighbour = y + j;


          //make sure coordinates are not out of bounds
          if (xNeighbour < 0){
            xNeighbour += w;
          }
          if(xNeighbour > w-1){
            xNeighbour -= w;
          }
          if(yNeighbour < 0){
            yNeighbour += h;
          }
          if(yNeighbour > h-1){
            yNeighbour -= h;
          }

          if(currentfield[calcIndex(w, xNeighbour, yNeighbour)] > 0){
            alive++;
          }
        }
      }
      //subtract middle cell
      if(currentfield[calcIndex(w, x, y)] > 0){
        alive--;
      }

      return alive;
}
 
void evolve(double* currentfield, double* newfield, int w, int h) {
  // int numberOfThreads = 4;
  // int threadLength = (w*h) / numberOfThreads;
  // if((w*h) % numberOfThreads > 0){
  //   threadLength++;
  // }
  // #pragma omp parallel firstprivate(threadLength, currentfield, w, h) shared(newfield) num_threads(numberOfThreads)
  // {
  // int x = (omp_get_thread_num() * threadLength) % w;
  // int y = ((omp_get_thread_num() * threadLength) - x) / w +1;
  // int xEnd = x + threadLength;
  // int yEnd = y;

  // //eliminate out of bounds cells for last thread
  // if(!(calcIndex(w, xEnd, yEnd) < w*h)){
  //   xEnd = w-1;
  //   yEnd = h-1;
  // }
  int x,y;
  for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) {
      
      //my code---------------------------------------------------------------------------------------------
      
      int aliveNeighbours = countNeighbours(currentfield, w, h, x, y);
      

      //rules
      if(aliveNeighbours == 3){
        newfield[calcIndex(w, x, y)] = 1;
      } else if(aliveNeighbours == 2){
        newfield[calcIndex(w, x, y)] = currentfield[calcIndex(w,x,y)] ;
      } else {
        newfield[calcIndex(w, x, y)] = 0;
      }
      //-----------------------------------------------------------------------------------------------------
      
    }
  }
  //}
}

 
void filling(double* currentfield, int w, int h) {
  int i;
  for (i = 0; i < h*w; i++) {
    currentfield[i] = (rand() < RAND_MAX / 10) ? 1 : 0; ///< init domain randomly
  }
  // currentfield[1] = 1;
  // currentfield[2] = 1;
  // currentfield[3] = 1;
  
}
 
void game(int w, int h) {
  double *currentfield = calloc(w*h, sizeof(double));
  double *newfield     = calloc(w*h, sizeof(double));
  
  //printf("size unsigned %d, size long %d\n",sizeof(float), sizeof(long));
  
  filling(currentfield, w, h);
  long t;
  for (t=0;t<TimeSteps;t++) {
    show(currentfield, w, h);
    
    evolve(currentfield, newfield, w, h);
    
    printf("%ld timestep\n",t);
    writeVTK2(t,currentfield,"gol", w, h);
    
    usleep(200000);

    //SWAP
    double *temp = currentfield;
    currentfield = newfield;
    newfield = temp;
  }
  
  free(currentfield);
  free(newfield);
  
}
 
int main(int c, char **v) {
  int w = 0, h = 0;
  if (c > 1) w = atoi(v[1]); ///< read width
  if (c > 2) h = atoi(v[2]); ///< read height
  if (w <= 0) w = 30; ///< default width
  if (h <= 0) h = 30; ///< default height
  game(w, h);
}
